import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import { Observable } from "rxjs/Rx";

@Injectable()
export class ConfigService {

  /**
   * Config object
   */
  private config: Object;

  /**
   *
   * @param {Http} http
   */
  constructor(
    private http: Http
  ) {}

  /**
   * Loads the config from data.json
   *
   * @returns {Promise<any>}
   */
  load() {
    return new Promise((resolve) => {
      this.http.get('/assets/data/data.json')
        .map(res => res.json())
        .catch((error: any) => {
          console.error("config not loaded: ", error);
          return Observable.throw(error.json().error || 'Server error');
        })
        .subscribe((data) => {
          this.config = data;
          console.log('config loaded: ', data);

          // ==== Initialize API ====
          // ---- get api base url ----
          let url = this.get('api_url');
          // ---- check if use api in production mode or not ----
          if (this.get('production') == false) {
            url += '/app_dev.php';
          }
          // ---- add api to call ----
          url += '/v1/public/initialization';
          this.http.get(url).subscribe(
            response => {
              console.log('INIT API');
              localStorage.setItem('api_data', JSON.stringify(response.json()));
              resolve();
            },
            error => {
              console.log('error initializing api');
            }
          );
        });
      });
  }

  /**
   * Gets a config value by key
   *
   * @param key
   * @returns {any}
   */
  public get(key: any) {
    return this.config[key];
  }
}

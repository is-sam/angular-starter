import { Injectable } from '@angular/core';
import { isArray } from 'util';

export interface LGFilter {
  type: string;            // filter type (simple, multi, date, hidden)
  name: string;            // filter name (api)
  value: string;           // filter value (api)
  displayName?: string;    // filter display name (front)
  displayValue?: string;   // filter display value (front)
}

@Injectable()
export class LGFiltersHandler {

  /**
   * list of selected filters
   */
  private selectedFilters: LGFilter[] = [];

  /**
   * opened list filter
   */
  private openedFilter: string = null;

  /**
   * if filter is applied or not
   * @type {boolean}
   */
  private filterApplied: boolean = false;

  /**
   * set true to keep filters on page destroy
   * @type {boolean}
   */
  private keepFilters: boolean = false;

  /**
   * on change event callback
   */
  private onChangeCallback?: Function;

  /**
   * class constructor
   */
  constructor() {}

  /**
   *
   * @returns {LGFilter[]}
   */
  public getSelectedFilters(hidden: boolean = false): LGFilter[] {
    if (hidden) {
      return this.selectedFilters;
    }
    return this.selectedFilters.filter((current) => {
      return current.type != 'hidden';
    });
  }

  /**
   * replace value of filter with same type in selected filters (simple select)
   *
   * @param {LGFilter} filter
   */
  public replaceFilter(filter: LGFilter): void {
    this.removeSameTypeFilters(filter);
    this.addFilter(filter);
  }

  /**
   * add value of filter to selected filters (multi select)
   *
   * @param {LGFilter} filter
   */
  public addFilter(filter: LGFilter): void {
    if (!this.filterExists(filter) && filter.value) {
      this.selectedFilters.push(filter);
    }
    if (this.onChangeCallback) {
      this.onChangeCallback(filter);
    }
    console.log(this.selectedFilters);
    this.closeOpenedFilter();
  }

  public getFilterDisplayName(name: string): string | null {
    let filterIndex: string = null;
    for (let index in this.selectedFilters) {
      if (name == this.selectedFilters[index]['name']) {
        filterIndex = index;
      }
    }
    if (filterIndex != null) {
      return this.selectedFilters[filterIndex]['displayValue'];
    }
    return null;
  }

  /**
   * remove filter from selected filters
   *
   * @param {LGFilter} filter
   */
  public removeFilter(filter: LGFilter): void {
    if (this.filterExists(filter)) {
      let index = this.findFilterIndex(filter);
      this.selectedFilters.splice(index, 1);
    }
  }

  /**
   * removes filter with same type
   *
   * @param {LGFilter} filter
   */
  public removeSameTypeFilters(filter: LGFilter): void {
    this.selectedFilters = this.selectedFilters.filter((current) => {
      return current.name != filter.name;
    });
  }

  /**
   * returns index of given filter in selected filters
   *
   * @param {LGFilter} filter
   * @returns {number}
   */
  public findFilterIndex(filter: LGFilter): number {
    return this.selectedFilters.findIndex(e => {
      return (e.name == filter.name) && (e.value == filter.value);
    });
  }

  /**
   * checks if filter exists in selected filters or not
   *
   * @param {LGFilter} filter
   * @returns {boolean}
   */
  public filterExists(filter: LGFilter): boolean {
    return this.findFilterIndex(filter) >= 0;

  }

  /**
   * clear selected filters list
   */
  public clearSelectedFilters(): void {
    if (!this.keepFilters) {
      this.selectedFilters = [];
      this.filterApplied = false;
    }
    this.keepFilters = false;
    this.closeOpenedFilter();
  }

  /**
   * open / close filter
   *
   * @param {string} name
   */
  public toggleOpenedFilter(name: string): void {
    if (this.openedFilter == name) {
      this.closeOpenedFilter();
    } else {
      this.openedFilter = name;
    }
  }

  /**
   * clears the opened filter
   */
  public closeOpenedFilter(): void {
    this.openedFilter = null;
  }

  /**
   * checks if filter with this name is opened
   *
   * @param {string} name
   * @returns {boolean}
   */
  public isFilterOpened(name: string): boolean {
    return this.openedFilter == name;

  }

  /**
   *
   */
  public applyFilters() {
    this.filterApplied = this.getSelectedFilters().length > 0;
  }

  /**
   *
   * @returns {boolean}
   */
  public enableValidateButton() {
    return this.getSelectedFilters().length == 0 && !this.filterApplied
  }

  /**
   *
   * @returns {Array}
   */
  public getFiltersParams(): Object {
    let params = {};
    for (let filter of this.selectedFilters) {
      if (params[filter.name]) { // param already exists => multiple values
        // trasform value to array
        if (!isArray(params[filter.name])) {
          params[filter.name] = [params[filter.name]];
        }
        params[filter.name].push(filter.value);
      } else { // first value of param
        params[filter.name] = filter.value;
      }
    }
    return params;
  }

  /**
   * keepFilters setter
   * @param value
   */
  public setKeepFilters(value: boolean) {
    this.keepFilters = value;
  }

  /**
   *
   * @param {Function} value
   */
  public setOnChangeCallback(value: Function) {
    this.onChangeCallback = value;
  }

  /**
   *
   * @param {string} name
   * @returns {LGFilter[]}
   */
  public getFilterByName(name: string) {
    return this.selectedFilters.filter(current => {
      return current.name == name;
    });
  }
}
import { Component, Input } from '@angular/core';
import { LGFiltersHandler } from './filters.handler';

@Component({
  selector: 'lg-filter',
  templateUrl: './filter.component.html'
})

/**
 * Filter component
 */
export class LGFilterComponent {

  /**
   * Filter type
   */
  @Input() type: string;

  /**
   * Filter name
   */
  @Input() name: string;

  /**
   * Filter display name
   */
  @Input() displayName: string;

  /**
   * Filter values list
   */
  @Input() filterList?: Array<any>;

  constructor(
    private filtersHandler: LGFiltersHandler
  ) {}
}
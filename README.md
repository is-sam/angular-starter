# Angular boilerplate example

In this example you have 3 files:

* **data.json**: contains the api configuration
    * api_url: the url of the api to use
    * api_timeout: timeout in ms
    * production: boolean whether to use the api in production mode or not
    
* **config.service.ts**:
    * reads the data.json and saves all the parameters so it can be used eveywhere in the app.
    * calls the api initialization url to get the initial api data and store it in the locale storage.
    
* **api.service.ts**: Overloads the Angular HttpService to match the way we use the api internally, it handles all the http calls with url parameters encoding and authentificaiton and more..

EDIT:

I have added a filter component with it's handler in the Filters directory:

* In the Component directory you can find the component that handles 3 types of filters: list (single or multiple choices) or date filter. 
* The filters.handler.ts handles the selected filters and provides a variaty of functions to interact with them.
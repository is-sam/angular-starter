import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../config/config.service';
import * as _ from "lodash";

@Injectable()
export class ApiService {

  /**
   * Stores the api url
   */
  private api_url: string;

  /**
   *
   * @param {Http} http
   * @param {ConfigService} config
   */
  constructor(
    private http: Http,
    private config: ConfigService
  ) {
    // ---- set api url from config ----
    this.api_url = this.config.get('api_url');
    // ---- use dev mode if production = false ----
    // if (!this.config.get('production') && !this.api_url.includes('app_dev.php')) {
    //   this.api_url += '/app_dev.php';
    // }
  }

  /**
   * Makes api calls
   *
   * @param {Array<string>} paUrl     Api url & method
   * @param poUrlData                 Url data
   * @param poBodyData                Body data
   * @param pbAuth                    use athentification or not
   * @returns {Observable<any>}
   */
  public request(paUrl: Array<string>, poUrlData: Object = null, poBodyData: Object = null, pbAuth: boolean = true): Observable<any> {
    // ==== Initialisations ====
    const method: string = paUrl[0];
    let url: string = this.api_url + paUrl[1];

    // ==== Format URL ====
    if (!_.isEmpty(poUrlData)) {
      url += url.includes('?') ? '&' : '?';
      url += this.urlEncodeData(poUrlData);
    }

    // ==== Create Headers ====
    const headers: Headers = new Headers();
    if (pbAuth) {
      headers.append('Authorization', 'Bearer ' + localStorage.getItem('jwt_token'));
    }
    if ('PUT' === method) {
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
    }

    // ==== Create Body ====
    const body: any = this.createBody(method, poBodyData);

    // ==== Create RequestOptions ====
    const options = new RequestOptions({
      method: method,
      headers: headers,
      body: body
    });

    // ==== Make api call and return result ====
    return this.http.request(url, options)
      .timeout(this.config.get('api_timeout') || 30000) // 30 sec timeout
      .map((response: Response) => {
        console.log(method + ' ' + url);
        const body = response.json();
        console.log('api response', body);
        return body;
      });
  }

  /**
   * Creates request body from method and data
   *
   * @param {string} psMethod
   * @param {Object} poData
   * @returns {any}
   */
  private createBody(psMethod: string, poData: Object): any {
    if ('GET' === psMethod) {
      return;
    }
    if ('POST' === psMethod) {
      return this.createFormData(poData);
    }
    return this.urlEncodeData(poData);
  }

  /**
   * Returns FormData object from poData argument
   *
   * @param {Object} poData
   * @returns {FormData}
   */
  private createFormData(poData: Object) {
    const formData: FormData = new FormData();
    for (const field in poData) {
      formData.append(field, poData[field]);
    }
    return formData;
  }

  /**
   * Returns urlencoded string from poData argument
   *
   * @param {Object} poData
   * @returns {string}
   */
  private urlEncodeData(poData: Object) {
    const laData = [];

    for (const field in poData) {
      laData.push(encodeURIComponent(field) + '=' + encodeURIComponent(poData[field]));
    }
    return laData.join('&');
  }

  /**
   *
   * @returns {Array<any>}
   */
  public getInitData() {
    return JSON.parse(localStorage.getItem('api_data'));
  }

  /**
   *
   * @param {string} psName
   * @returns {Array<string>}
   */
  public getLink(psName: string) {
    // TODO: add protection
    let links: Array<any> = JSON.parse(localStorage.getItem('api_data'))._links;
    return links[psName];
  }
}
